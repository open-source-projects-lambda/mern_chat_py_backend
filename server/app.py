from fastapi import Cookie, FastAPI, HTTPException, Request,Depends,status,File, UploadFile, WebSocket,  WebSocketDisconnect
from fastapi.responses import JSONResponse,PlainTextResponse
from fastapi.encoders import jsonable_encoder
from fastapi.middleware.cors import CORSMiddleware
from fastapi.security import OAuth2PasswordRequestForm, OAuth2PasswordBearer,HTTPBearer,HTTPAuthorizationCredentials
from passlib.hash import bcrypt
from pydantic import BaseModel
from datetime import datetime, timedelta
from .models import User,Message
from .database import client
import websockets
import jwt
from jose import jwt,JWTError
from decouple import config
from typing import Optional,List
import asyncio
import os
import base64
import uuid
from .database import users_collection,db,messages_collection
import json


app = FastAPI()


UPLOAD_DIRECTORY = "uploads"
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")
# jwt config
jwt_secret = config("JWT_SECRET").encode()
bearer_scheme = HTTPBearer()


origins = config("CLIENT_URL")
# enable CORS for all origins
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["GET","PUT","PATCH","DELETE","POST"],
    allow_headers=["*"],
)

class LoginRequest(BaseModel):
    username: str
    password: str


class LoginResponse(BaseModel):
    access_token: str
    token_type: str
    username: str



# Define the WebSocket connection manager
class ConnectionManager:
    def __init__(self):
        self.active_connections = []



@app.post("/uploadfile/")
async def create_upload_file(file: UploadFile):
    try:
        file_bytes = await file.read()
        file_key = f"{file.filename}"
        s3.upload_fileobj(BytesIO(file_bytes), BUCKET_NAME, file_key)
        file_url = f"https://{BUCKET_NAME}.s3.amazonaws.com/{file_key}"
        return {"file_url": file_url}
    except ClientError as e:
        print(e)
        return {"detail": "Failed to upload file."}
class UserRegistrationRequest(BaseModel):
    username: str
    password: str



def generate_token(user):
    token_payload = {
        "user_id": str(user._id),
        "username": user.username,
        "exp": datetime.utcnow() + timedelta(hours=1),
    }
    return jwt.encode(token_payload, jwt_secret, algorithm="HS256")


# test sync
@app.get('/test', tags=["Test"])
async def test():
    return "doubt"


# Register endpoint
@app.post('/register', tags=["Register"])
async def create_user(request: UserRegistrationRequest):
    user = await User.create({"username": request.username, "password": request.password})
    response = JSONResponse(content={"id": str(user._id)})
    token = generate_token(user)
    response.set_cookie(key="token", value=token, httponly=True, samesite="none", secure=True)
    return response


@app.post('/login', response_model=LoginResponse)
async def login(request: Request, login_request: LoginRequest):
    # find user in database
    # assuming User is a model representing your user collection/table
    find_user = await User.find_one({"username": login_request.username})

    if find_user:
        # extract password hash from the dictionary object
        password_hash = find_user.get("password_hash")

        # check if password matches
        pass_ok = bcrypt.verify(login_request.password, password_hash)

        if pass_ok:
            # generate and return JWT token
            token_payload = {"user_id": str(find_user["_id"]), "username": login_request.username}
            token = jwt.encode(token_payload, jwt_secret, algorithm="HS256")
            response = LoginResponse(access_token=token, token_type="bearer", username=login_request.username)
            return JSONResponse(content=response.dict(), headers={"Set-Cookie": "access_token={}; HttpOnly".format(token)})
        else:
            raise HTTPException(status_code=401, detail="Incorrect username or password")
    else:
        raise HTTPException(status_code=401, detail="Incorrect username or password")



# user profile section
@app.get("/profile")
async def profile(request):
    token = request.cookies.get("access_token")
    if not token:
        token = request.headers.get("Authorization")
        if token:
            token = token.split("Bearer ")[1]
        else:
            raise HTTPException(status_code=401, detail="No token")

    try:
        payload = jwt.decode(token, jwt_secret, algorithms="HS256")
    except JWTError:
        raise HTTPException(status_code=401, detail="Invalid token")
    
    return JSONResponse(content=payload)


@app.get('/people')
async def get_people():
    users = await users_collection.find({}, {'_id':1,'username':1}).to_list(length=None)
    return users



async def connect(self, websocket: WebSocket, user_id: str, username: str):
    await websocket.accept()
    self.active_connections.append({"websocket": websocket, "user_id": user_id, "username": username})
    await self.broadcast_active_users()

async def disconnect(self, websocket: WebSocket):
        self.active_connections.remove({"websocket": websocket})
        await self.broadcast_active_users()

async def broadcast_active_users(self):
        active_usernames = [connection["username"] for connection in self.active_connections]
        active_user_ids = [connection["user_id"] for connection in self.active_connections]
        message = {"type": "active_users", "usernames": active_usernames, "user_ids": active_user_ids}
        for connection in self.active_connections:
            await connection["websocket"].send_json(message)

async def send_message(self, message: dict, recipient_id: str):
        for connection in self.active_connections:
            if connection["user_id"] == recipient_id:
                await connection["websocket"].send_json(message)

async def get_user_data_from_request(request: Request) -> User:
    credentials: HTTPAuthorizationCredentials = await bearer_scheme(request)
    if not credentials:
        raise HTTPException(status_code=401, detail="Not authenticated")

    token = request.headers.get("Authorization")
    if token:
        token = token.split("Bearer")[1]
    else:
        raise HTTPException(status_code=401, detail="No token")
    try:
        token_payload = jwt.decode(token, JWT_SECRET, algorithms=["HS256"])
        return User(user_id=token_payload["user_id"], username=token_payload["username"])
    except jwt.PyJWTError:
        raise HTTPException(status_code=401, detail="Invalid token")


async def notify_about_online_people():
    online_users = []
    for client in connected_clients:
        online_users.append({"userId": client["userId"], "username": client["username"]})
    message = {"online": online_users}
    for client in connected_clients:
        await client["websocket"].send(json.dumps(message))

async def start_ping(websocket: websockets.WebSocketClientProtocol):
    while websocket.open:
        try:
            await websocket.ping()
            await asyncio.sleep(5)
        except:
            websocket.open = False
            await websocket.close()
# 


# Messages endpoint
@app.get('/messages/{userId}')
async def messages(userId: str, current_user: User = Depends(get_user_data_from_request)):
    messages = await db['messages'].find({
       "sender": {"$in": [userId, current_user.user_id]},
       "recipient": {"$in": [userId, current_user.user_id]},
    }).sort("created_at", 1)

    return [Message(**msg) for msg in messages]

#
# # Create an instance of the WebSocket connection manager
connection_manager = ConnectionManager()

# Define the WebSocket endpoint
@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket, request: Request, current_user: dict = Depends(get_user_data_from_request)):
    await connection_manager.connect(websocket, current_user["user_id"], current_user["username"])
    try:
        while True:
            message = await websocket.receive_text()
            try:
                message_data = json.loads(message)
                recipient_id = message_data["recipient_id"]
                text = message_data.get("text")
                file_data = message_data.get("file_data")
                filename = None
                if file_data:
                    data = file_data.split(',')[1]
                    file_ext = file_data.split(',')[0].split(';')[0].split('/')[1]
                    filename = f"{str(uuid.uuid4())}.{file_ext}"
                    file_path = os.path.join(UPLOAD_DIRECTORY, filename)
                    with open(file_path, "wb") as f:
                        f.write(base64.b64decode(data))
                message_dict = {"type": "message", "text": text, "sender_id": current_user["user_id"], "recipient_id": recipient_id, "file": filename}
                await connection_manager.send_message(message_dict, recipient_id)
            except Exception as e:
                error_dict = {"type": "error", "message": str(e)}
                await websocket.send_json(error_dict)
    except WebSocketDisconnect:
        connection_manager.disconnect(websocket)